var app = require('./config/apiConfig');
//var dados = require('./models/dados');
var dadosController = require('./controllers/dadosController');

app.get('/', function(req,res){
    res.end('API - Pontos de Onibus do Rio de Janeiro');
});

app.get('/pontos/:linha', function(req,res){
    var linha = req.params.linha;
    dadosController.getPontos(linha, function(resp){
        res.json(resp);
    });
});