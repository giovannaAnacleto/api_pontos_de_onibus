var request = require('request');

var retorno;
request('http://pgeo3.rio.rj.gov.br/arcgis/rest/services/Transporte_Trafego/Transporte_Publico/MapServer/24/query?where=1=1&outFields=*&outSR=4326&f=json', function(err,res,body){
    retorno = body;
});

exports.getPontos = function(numLinha){
    var objeto = JSON.parse(retorno);
    var lista;

    for(var i = 0; i < objeto.features.length; i++){
        if(objeto.features[i].attributes.NumLinha == numLinha){
            return objeto.features[i].geometry.points;
        }else{
            continue;
        }
    }
};