var express = require('express');
var bodyParser = require('body-parser');

var port = '8400';
var app = module.exports = express();

app.listen(port);
app.use(bodyParser.urlencoded({encoded:true}));
app.use(bodyParser.json()); 

app.use(function(req,res,next){
    res.setHeader('Acces-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    next();
});
